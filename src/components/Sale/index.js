import { Component } from "react";

class Sale extends Component {
  render() {
    return (
      <div style={{ color: "red" }}>
        Desconto Aplicado: {this.props.discount}%
      </div>
    )
  }
}

export default Sale;