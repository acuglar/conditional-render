import { Component } from 'react';
import Sale from '../Sale';

class ProductList extends Component {
  render() {
    const { list } = this.props;  //param list invocado em App
    return (
      <div>
        {list.map((prod, index) => {
          return (
            <ul key={index}>
              <li>Produto: {prod.name}</li>
              <li>$: {prod.price && prod.discountPercentage ? (prod.price * (1 - prod.discountPercentage / 100)).toFixed(2) : prod.price}</li>
              {prod.discountPercentage && <Sale discount={prod.discountPercentage} />}
            </ul>
          )
        })
        }
      </div>
    )
  }
}

export default ProductList;