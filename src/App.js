import React from 'react';
import './App.css';
import ProductList from './components/ProductList/index.js'
import productList from './components/ProductList/helper.js'

class App extends React.Component {

  render() {
    console.log(productList)
    return (
      <div className="App">
        <header className="App-header">
          <ProductList list={productList} />
        </header>
      </div>
    );
  }
}

export default App;
